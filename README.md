# Live site
https://gnu.my.id

# Demo site

URL | Powered by
---------|----------
 https://gnu.ertomedia.net | [Render](https://gnu.onrender.com/)
 https://gnu.erto.co | [Firebase](https://gnumyid.web.app/)
 https://gnumyid.netlify.app | [Netlify](https://gnumyid.netlify.app/)
 https://manual.now.sh | [Vercel](https://manual.now.sh/)
